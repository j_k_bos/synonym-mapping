_The use of this script is explained in the Readme in the parent directory._

`cities.sh` converts all CSV files to text files first, omitting the first line and cutting the first field. Second, the script
makes several modification to the files:

- The script adds the name of the province itself to each file.
- The script adds location Gasselternijveenschemond to Drenthe.
- The script adds location Almere to Flevoland.
- The script adds synonyms Frysl�n and Fryslan to Friesland.
- The script adds synonym Brabant to North Brabant.
- The script deletes location Nederland from Overijssel.
- The script adds locations Den Haag and The Hague to South Holland.