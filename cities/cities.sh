#!/bin/bash

overijssel=$(mktemp)

# omit first line, cut first field and save result to corresponding file
tail -n +2 drenthe.csv | cut -d"," -f1 > drenthe.txt
tail -n +2 flevoland.csv | cut -d"," -f1 > flevoland.txt
tail -n +2 friesland.csv | cut -d"," -f1 > friesland.txt
tail -n +2 gelderland.csv | cut -d"," -f1 > gelderland.txt
tail -n +2 groningen.csv | cut -d"," -f1 > groningen.txt
tail -n +2 limburg.csv | cut -d"," -f1 > limburg.txt
tail -n +2 north_brabant.csv | cut -d"," -f1 > north_brabant.txt
tail -n +2 north_holland.csv | cut -d"," -f1 > north_holland.txt
tail -n +2 overijssel.csv | cut -d"," -f1 > $overijssel
tail -n +2 south_holland.csv | cut -d"," -f1 > south_holland.txt
tail -n +2 utrecht.csv | cut -d"," -f1 > utrecht.txt
tail -n +2 zeeland.csv | cut -d"," -f1 > zeeland.txt

# modifications to files
(echo "drenthe" && echo "gasselternijveenschemond") >> drenthe.txt
(echo "flevoland" && echo "almere") >> flevoland.txt
# (don't forget proud fryslân)
(echo "friesland" && echo "fryslân" && echo "fryslan") >> friesland.txt
echo "gelderland" >> gelderland.txt
echo "groningen" >> groningen.txt
echo "limburg" >> limburg.txt
(echo "noord-brabant" && echo "brabant") >> north_brabant.txt
echo "noord-holland" >> north_holland.txt

echo "overijssel" >> $overijssel
sed 's/Nederland//g' $overijssel > overijssel.txt

(echo "zuid-holland" && echo "den haag" && echo "the hague") >> south_holland.txt

echo "utrecht" >> utrecht.txt
echo "zeeland" >> zeeland.txt

# remove CSV files
rm drenthe.csv flevoland.csv friesland.csv gelderland.csv \
groningen.csv limburg.csv north_brabant.csv north_holland.csv \
overijssel.csv south_holland.csv utrecht.csv zeeland.csv 

# remove temporary file
rm "$overijssel" 
