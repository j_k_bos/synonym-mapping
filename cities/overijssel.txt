Aadorp
Achterhoek
Albergen
Almelo
Ane
Anerveen
Anevelde
Ankum
Apenhuizen
Archem
Arriën
Arriërveld
Averlo
Azelo
Baarlo
Baarlo
Baars
Balkbrug
Barsbeek
Basse
Basserveld
Bathmen
Bavinkel
Beckum
Beekdorp
Beerze
Beerzerveld
Belt-Schutsloot
Benedenvaart
Bentelo
Bergentheim
Berghum
't Bergje
Besthmen
"Beuningen
Beuseberg
Bisschopswetering
Blankenham
Blokzijl
Boekelo
Boerhaar
Borkeld
Borne
Bornerbroek
Boskamp
Braamberg
Brammelo
Breklenkamp
Brinkhoek
Broekheurne
Broekhuizen
Broekland
Brucht
Bruchterveld
Bruggenhoek
Bruinehaar
Buurse
Cellemuiden
Collendoorn
Collendoornerveen
Colmschate
Daarle
Daarlerveen
Dalfsen
Dalmsholte
De Belt
De Bult
Dedemsvaart
De Haandrik
De Haar
De Heuvels
De Hooge Wegen
De Kolonie
De Krieger
De Krim
Delden
Deldenerbroek
Deldeneresch
De Leijen
De Lutte
De Maat
De Marshoek
De Meele
De Meene
De Mulderij
Den Braam
Denekamp
Den Ham
Den Huizen
Den Hulst
Den Kaat
Den Nul
Den Oosterhuis
Den Velde
Den Westerhuis
De Pol
De Pol
De Pollen
De Roskam
De Schans
Deurningen
De Velde
Deventer
De Zande
Diepenheim
Diepenveen
Diffelen
Dijkerhoek
Doosje
Dortherhoek
Driene
Dulder
Duur
Dwarsgracht
Ebbenbroek
Eerde
Eese
Eesveen
Egede
Eikelhof
Eelen en Rhaan
Elsen
Elsenerbroek
Elshof
Emmen
Engeland
Engeland
Enschede
Enter
Eppenzolder
Espelo
Fleringen
Fortmond
Frankhuis
Frieswijk
Gammelke
Geerdijk
Geesteren
Genemuiden
Genne
Genne-Overwaters
Gerner
Giethmen
Giethoorn
Glane
Glane-Beekhoek
Glanerbrug
Goor
Gouden Ploeg
Grafhorst
Gramsbergen
Groot Agelo
Groot-Oever
Haaksbergen
't Haantje
Haarle
Haarle
Haerst
Halfweg
Hallerhoek
Hamingen
Hanekamp
Hankate
Harbrinkhoek
Hardenberg
Harmöle
Hasselt
Heemserveen
's-Heerenbroek
Heeten
Heetveld
Heino
Hellendoorn
Hengelo
Hengevelde
Hengforden
Herfte
Herike
Hertme
Herxen
Hessum
Het Loo
Het Stift
Hexel
Hezingen
Hoge-Hexel
Hogeweg
Holt
Holten
Holten
Holterbroek
Holtheme
Holthone
Honesch
Hoogengraven
Hoogenweg
Hoonhorst
Hulsen
Huurne
IJhorst
IJpelo
IJsselham
IJsselmuiden
Jonen
Junne
Kadoelen
Kalenberg
Kallenkote
Kampen
Kampereiland
Kamperveen
Kamperzeedijk-Oost
Kamperzeedijk-West
Katerveer
Keiendorp
Kievitshaar
Kievitsnest
Klein Agelo
Kloosterhaar
Klössehoek
Kuinre
Laag Zuthem
Langelo
Langenholte
Langeveen
Lankhorst
Lattrop
Leeuwte
Lemele
Lemelerveld
Lemselo
Lenthe
Lettele
Lichtmis
Lierderholthuis
Ligtenberg
Linde
Linde
Lonneker
Loo
Look
Losser
Lutten
Luttenberg
Lutten-Oever
Lutterhartje
Magele
Mander
Manderveen
Mariaparochie
Mariënberg
Mariënheem
Marijenkampen
Markelo
Markvelde
Marle
Marle
Mastenbroek
Mataram
Meer
Mekkelhorst
Middel
Millingen
Moespot
Molenbelt
Molenhoek
Muggenbeet

Neerdorp
Nieuwebrug
Nieuwe Wetering
Nieuw-Heeten
Nieuwleusen
Nieuwstad
Nijrees
Nijverdal
Noetsele
Noord Deurningen
Noordijk
Noord-Meer
Noord-Stegeren
Notter
Nutter
Oele
Okkenbroek
Oldemarkt
Oldeneel
Oldenzaal
Olst
Ommen
Ommerschans
Onna
Ooster-Dalfsen
Oosterholt
Ootmarsum
Ossenzijl
Oud-Avereest
Oud-Bergentheim
Oude Molen
Oudleusen
Oud-Lutten
Oud Ootmarsum
Overdinkel
Overwater
Oxe
Paasloo
Pieriksmars
Piksen
Poepershoek
Pothoek
Punthorst
Raalte
Radewijk
Rande
Rechteren
Rectum
Reutum
Rhaan
Rheeze
Rheezerveen
Rijssen
Roebolligehoek
Roekebosch
Rollecate
Ronduite
Rosengaarde
Rossum
Rouveen
Ruitenveen
Rutbeek
Saasveld
Schalkhaar
Scheerwolde
Schoolbuurt
Schuilenburg
Schuinesloot
Sibculo
Sint Isidorushoeve
Sint Jansklooster
Slagharen
Slingenberg
Sluis Zeven
Snippeling
Sponturfwijk
Spoolde
Staphorst
Steenwijk
Steenwijkerwold
Stegeren
Stegerveld
Stepelo
Stokkum
Streukel
Strooiendorp
Thij
Tilligte
Tjoene
Tubbergen
Tuk
Tusveld
Twekkelo
Usselo
Varsen
Vasse
Veecaten
Veldhoek
Venebrugge
Vennenberg
Vilsteren
Vinkenbuurt
Vollenhove
Volthe
Vriezenveen
Vroomshoop
Wanneperveen
Weerselo
Weitemanslanden
Weleveld
Welsum
Welsum
Welsumerveld
Wesepe
Westeinde
Westerhaar-Vriezenveensewijk
West Geesteren
Wetering
Wetserhuizingerveld
Wiene
Wierden
Wijhe
Wijthmen
Willemsoord
Wilsum
Windesheim
Witharen
Witman
Witte Paarden
Zalk
Zalné
Zeesse
Zeldam
Zenderen
Zoeke
Zuideinde
Zuidloo
Zuidveen
Zuna
Zwartewatersklooster
Zwartsluis
Zwolle
overijssel
