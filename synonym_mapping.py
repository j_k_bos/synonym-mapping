import string
import sys


def initialize_synonyms():
    """returns the synonyms initialized"""
    # replace ranja or limonade with other words
    # NOTE: use lowercase
    synonym_1 = "ranja"
    synonym_2 = "limonade"

    return synonym_1, synonym_2


def initialize_areas(friesland_s1, groningen_s1, drenthe_s1, overijssel_s1,
                     gelderland_s1, north_brabant_s1, limburg_s1,
                     north_holland_s1, south_holland_s1, flevoland_s1,
                     utrecht_s1, zeeland_s1, friesland_s2, groningen_s2,
                     drenthe_s2, overijssel_s2, gelderland_s2,
                     north_brabant_s2, limburg_s2, north_holland_s2,
                     south_holland_s2, flevoland_s2, utrecht_s2,
                     zeeland_s2):
    """returns the areas initialized"""
    # replace East and West with other words
    area1 = "East"
    area2 = "West"

    # replace names of provinces with other provinces, keeping _s1 and _s2
    # at the end of evert province, e.g. _s1 stands for synonym_1.
    # example: area1_s1 = (north_holland_s1 + drenthe_s1)
    area1_s1 = (friesland_s1 + groningen_s1 + drenthe_s1 + overijssel_s1 +
                gelderland_s1 + north_brabant_s1 + limburg_s1)
    area1_s2 = (friesland_s2 + groningen_s2 + drenthe_s2 + overijssel_s2 +
                gelderland_s2 + north_brabant_s2 + limburg_s2)

    area2_s1 = (north_holland_s1 + south_holland_s1 + flevoland_s1 +
                utrecht_s1 + zeeland_s1)
    area2_s2 = (north_holland_s2 + south_holland_s2 + flevoland_s2 +
                utrecht_s2 + zeeland_s2)

    return area1, area2, area1_s1, area1_s2, area2_s1, area2_s2


def city_set_generator():
    """returns set with cities for each corresponding province,
       deleting all interpunction and lowering all cities"""
    with open('cities/drenthe.txt') as drenthe_file:
        drenthe = {row.translate(str.maketrans('', '', string.punctuation))
                   .split(",")[0].strip().lower() for row in drenthe_file}

    with open('cities/flevoland.txt') as flevoland_file:
        flevoland = {row.translate(str.maketrans('', '', string.punctuation))
                     .split(",")[0].strip().lower() for row in flevoland_file}

    with open('cities/friesland.txt') as friesland_file:
        friesland = {row.translate(str.maketrans('', '', string.punctuation))
                     .split(",")[0].strip().lower() for row in friesland_file}

    with open('cities/gelderland.txt') as gelderland_file:
        gelderland = \
            {row.translate(str.maketrans('', '', string.punctuation))
             .split(",")[0].strip().lower() for row in gelderland_file}

    with open('cities/groningen.txt') as groningen_file:
        groningen = {row.translate(str.maketrans('', '', string.punctuation))
                     .split(",")[0].strip().lower() for row in groningen_file}

    with open('cities/limburg.txt') as limburg_file:
        limburg = {row.translate(str.maketrans('', '', string.punctuation))
                   .split(",")[0].strip().lower() for row in limburg_file}

    with open('cities/north_brabant.txt') as north_brabant_file:
        north_brabant = \
            {row.translate(str.maketrans('', '', string.punctuation))
             .split(",")[0].strip().lower() for row in north_brabant_file}

    with open('cities/north_holland.txt') as north_holland_file:
        north_holland = \
            {row.translate(str.maketrans('', '', string.punctuation))
             .split(",")[0].strip().lower() for row in north_holland_file}

    with open('cities/overijssel.txt') as overijssel_file:
        overijssel = \
            {row.translate(str.maketrans('', '', string.punctuation))
             .split(",")[0].strip().lower() for row in overijssel_file}

    with open('cities/south_holland.txt') as south_holland_file:
        south_holland = \
            {row.translate(str.maketrans('', '', string.punctuation))
             .split(",")[0].strip().lower() for row in south_holland_file}

    with open('cities/utrecht.txt') as utrecht_file:
        utrecht = {row.translate(str.maketrans('', '', string.punctuation))
                   .split(",")[0].strip().lower() for row in utrecht_file}

    with open('cities/zeeland.txt') as zeeland_file:
        zeeland = {row.translate(str.maketrans('', '', string.punctuation))
                   .split(",")[0].strip().lower() for row in zeeland_file}

    return (drenthe, flevoland, friesland, gelderland, groningen,
            limburg, north_brabant, north_holland, overijssel,
            south_holland, utrecht, zeeland)


def in_province(user_place, province):
    """returns True if place equals city in province"""
    for place in province:
        if place == user_place[0]:
            return True


def main():

    (drenthe, flevoland, friesland, gelderland, groningen,
     limburg, north_brabant, north_holland, overijssel,
     south_holland, utrecht, zeeland) = city_set_generator()

    synonym_1, synonym_2 = initialize_synonyms()

    # initialize synonyms counters
    result = {"drenthe_s1": 0, "drenthe_s2": 0, "flevoland_s1": 0,
              "flevoland_s2": 0, "friesland_s1": 0, "friesland_s2": 0,
              "gelderland_s1": 0, "gelderland_s2": 0, "groningen_s1": 0,
              "groningen_s2": 0, "limburg_s1": 0, "limburg_s2": 0,
              "north_brabant_s1": 0, "north_brabant_s2": 0,
              "north_holland_s1": 0, "north_holland_s2": 0,
              "overijssel_s1": 0, "overijssel_s2": 0, "south_holland_s1": 0,
              "south_holland_s2": 0, "utrecht_s1": 0, "utrecht_s2": 0,
              "zeeland_s1": 0, "zeeland_s2": 0, "unknown_s1": 0,
              "unknown_s2": 0}

    for line in sys.stdin:
        # lower words in place and tweet and delete all punctuation
        place = line.split("\t")[0].lower()\
            .translate(str.maketrans('“”', '  ', string.punctuation))\
            .split()
        tweet = line.split("\t")[1].lower()\
            .translate(str.maketrans('“”', '  ', string.punctuation)).\
            split()
        # improve efficiency
        if place:
            # allow cities to contain up to two words
            for word in ["de", "het", "t", "groot", "grote",
                         "s", "den", "sint", "the"]:
                if word == place[0]:
                    place[0] = place[0] + " " + place[1]
            # exclude retweets and tweets from Belgium
            if "belgie" not in place and \
            "belgië" not in place and not tweet[0] == "rt":
                tweet = ''.join(tweet)
                # exclude tweets containing both synonyms
                if synonym_2 in tweet and synonym_1 in tweet:
                    pass
                else:
                    if in_province(place, drenthe):
                        if synonym_2 in tweet:
                            result["drenthe_s1"] += 1
                        elif synonym_1 in tweet:
                            result["drenthe_s2"] += 1
                    elif in_province(place, flevoland):
                        if synonym_2 in tweet:
                            result["flevoland_s1"] += 1
                        elif synonym_1 in tweet:
                            result["flevoland_s2"] += 1
                    elif in_province(place, friesland):
                        if synonym_2 in tweet:
                            result["friesland_s1"] += 1
                        elif synonym_1 in tweet:
                            result["friesland_s2"] += 1
                    elif in_province(place, gelderland):
                        if synonym_2 in tweet:
                            result["gelderland_s1"] += 1
                        elif synonym_1 in tweet:
                            result["gelderland_s2"] += 1
                    elif in_province(place, groningen):
                        if synonym_2 in tweet:
                            result["groningen_s1"] += 1
                        elif synonym_1 in tweet:
                            result["groningen_s2"] += 1
                    elif in_province(place, limburg):
                        if synonym_2 in tweet:
                            result["limburg_s1"] += 1
                        elif synonym_1 in tweet:
                            result["limburg_s2"] += 1
                    elif in_province(place, north_brabant):
                        if synonym_2 in tweet:
                            result["north_brabant_s1"] += 1
                        elif synonym_1 in tweet:
                            result["north_brabant_s2"] += 1
                    elif in_province(place, north_holland):
                        if synonym_2 in tweet:
                            result["north_holland_s1"] += 1
                        elif synonym_1 in tweet:
                            result["north_holland_s2"] += 1
                    elif in_province(place, overijssel):
                        if synonym_2 in tweet:
                            result["overijssel_s1"] += 1
                        elif synonym_1 in tweet:
                            result["overijssel_s2"] += 1
                    elif in_province(place, south_holland):
                        if synonym_2 in tweet:
                            result["south_holland_s1"] += 1
                        elif synonym_1 in tweet:
                            result["south_holland_s2"] += 1
                    elif in_province(place, utrecht):
                        if synonym_2 in tweet:
                            result["utrecht_s1"] += 1
                        elif synonym_1 in tweet:
                            result["utrecht_s2"] += 1
                    elif in_province(place, zeeland):
                        if synonym_2 in tweet:
                            result["zeeland_s1"] += 1
                        elif synonym_1 in tweet:
                            result["zeeland_s2"] += 1
                    else:
                        if synonym_2 in tweet:
                            result["unknown_s1"] += 1
                        elif synonym_1 in tweet:
                            result["unknown_s2"] += 1

        else:
            if not tweet[0] == "rt":
                tweet = ''.join(tweet)
                if synonym_2 in tweet:
                    result["unknown_s1"] += 1
                elif synonym_1 in tweet:
                    result["unknown_s2"] += 1

    friesland_s1 = result["friesland_s1"]
    groningen_s1 = result["groningen_s1"]
    drenthe_s1 = result["drenthe_s1"]
    overijssel_s1 = result["overijssel_s1"]
    gelderland_s1 = result["gelderland_s1"]
    north_brabant_s1 = result["north_brabant_s1"]
    limburg_s1 = result["limburg_s1"]
    north_holland_s1 = result["north_holland_s1"]
    south_holland_s1 = result["south_holland_s1"]
    flevoland_s1 = result["flevoland_s1"]
    utrecht_s1 = result["utrecht_s1"]
    zeeland_s1 = result["zeeland_s1"]

    friesland_s2 = result["friesland_s2"]
    groningen_s2 = result["groningen_s2"]
    drenthe_s2 = result["drenthe_s2"]
    overijssel_s2 = result["overijssel_s2"]
    gelderland_s2 = result["gelderland_s2"]
    north_brabant_s2 = result["north_brabant_s2"]
    limburg_s2 = result["limburg_s2"]
    north_holland_s2 = result["north_holland_s2"]
    south_holland_s2 = result["south_holland_s2"]
    flevoland_s2 = result["flevoland_s2"]
    utrecht_s2 = result["utrecht_s2"]
    zeeland_s2 = result["zeeland_s2"]

    area1, area2, area1_s1, area1_s2, area2_s1, area2_s2 = \
        (initialize_areas(friesland_s1, groningen_s1, drenthe_s1,
                          overijssel_s1, gelderland_s1, north_brabant_s1,
                          limburg_s1, north_holland_s1, south_holland_s1,
                          flevoland_s1, utrecht_s1, zeeland_s1, friesland_s2,
                          groningen_s2, drenthe_s2, overijssel_s2,
                          gelderland_s2, north_brabant_s2, limburg_s2,
                          north_holland_s2, south_holland_s2, flevoland_s2,
                          utrecht_s2, zeeland_s2))

    print("{0:>25}{1:>7}\n"
          "Drenthe:{2:>10}{3:>10}\n"
          "Flevoland:{4:>8}{5:>10}\n"
          "Friesland:{6:>8}{7:>10}\n"
          "Gelderland:{8:>7}{9:>10}\n"
          "Groningen:{10:>8}{11:>10}\n"
          "Limburg:{12:>10}{13:>10}\n"
          "North Brabant:{14:>4}{15:>10}\n"
          "North Holland:{16:>4}{17:>10}\n"
          "Overijssel:{18:>7}{19:>10}\n"
          "South Holland:{20:>4}{21:>10}\n"
          "Utrecht:{22:>10}{23:>10}\n"
          "Zeeland:{24:>10}{25:>10}\n"
          "Unknown:{26:>10}{27:>10}"
          .format(synonym_2, synonym_1,
                  result["drenthe_s1"], result["drenthe_s2"],
                  result["flevoland_s1"], result["flevoland_s2"],
                  result["friesland_s1"], result["friesland_s2"],
                  result["gelderland_s1"], result["gelderland_s2"],
                  result["groningen_s1"], result["groningen_s2"],
                  result["limburg_s1"], result["limburg_s2"],
                  result["north_brabant_s1"], result["north_brabant_s2"],
                  result["north_holland_s1"], result["north_holland_s2"],
                  result["overijssel_s1"], result["overijssel_s2"],
                  result["south_holland_s1"], result["south_holland_s2"],
                  result["utrecht_s1"], result["utrecht_s2"],
                  result["zeeland_s1"], result["zeeland_s2"],
                  result["unknown_s1"], result["unknown_s2"]))

    print("\n{0:>22}{1:>7}\n"
          "{2}:{3:>10}{4:>10}\n"
          "{5}:{6:>10}{7:>10}\n"
          .format(synonym_2, synonym_1, area1, area1_s1,
                  area1_s2, area2, area2_s1, area2_s2))


if __name__ == "__main__":
    main()
