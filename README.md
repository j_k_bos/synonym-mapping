# Repository for the work described in _Synonym mapping in the Netherlands using geography: limonade versus ranja_. 
This repository offers scripts to automatically map the use of two synonyms per province in the Netherlands using 
Twitter data from January 1 to June 30, 2018.

## Quick Start

[Following the Twitter guidelines](https://developer.twitter.com/en/developer-terms/policy.html), users of Karora are allowed to
export up to 50,000 tweet object per day. However, we are not allowed to publish data considering this project. Therefore, the following approach is needed to generate results. The estimated time of running this script is 4 hours.
By means of experimenting with these scripts, exporting is recommended. This will reduce the running time drastically.
However, no tools for doing so are provided in this repository. Meeting Twitter guidelines is namely strongly depended on the
chosen synonyms.

### Requirements:
- Access to corpora on Karora, a machine which the University of Groningen owns.
- Ubuntu

### Experiment conducted using:
- Python 3.6.7
- Ubuntu 18.04.2 LTS

## Replicating: tl;dr
_To conduct a similar experiment or to replicate step by step, __go to Step-by-Step Guide___.  
__(1)__ Log in into Karora, open the terminal and cd to your personal directory.  
__(2)__ Clone this repository using command `$ git clone https://j_k_bos@bitbucket.org/j_k_bos/synonym-mapping.git`.  
__(3)__ Cd to folder `synonym-mapping`.  
__(4)__ Give execute permissions to `2018_01-06.sh` using command `$ chmod +x 2018_01-06.sh`. This script feeds the program.  
__(5)__ Run the script and program using command `$ ./2018_01-06.sh | python3 synonym_mapping.py`.  


## Step-by-Step Guide
## Part One: Initializing 2018_01-06.sh
__(1)__ After logging in into Karora, open the terminal, __(2)__ cd to your personal directory and __(3)__ download `2018_01-06.sh`.
We will use this script to feed the program. __(4)__ Give execute permissions to the downloaded script using `$ chmod +x 2018_01-06.sh`.
__(5)__ Download `synonym_mapping.py` to your personal directory as well.
  
When using other synonyms than _ranja_ or _limonade_, __continue reading__. By replicating this experiment, __go to Part Three__.

__(6)__ Open `2018_01-06.sh`. The following code is needed: 

`Initialize synonyms`  
`synonym1=$"limonade"`  
`synonym2=$"ranja"`  

__(7)__ Substitute _limonade_ and _ranja_ by the desired synonyms - preserving the quotation marks - and save the file.

## Part Two: Initializing synonym_mapping.py
__(8)__ Open `synonym_mapping.py`. By default, the program maps _ranja_ and _limonade_. In order to change these variables,
functions `initialize_synonyms` and eventually `initialize_areas` need to be changed. __(9)__ Substitute _ranja_ and _limonade_ of
`def initialize_synonyms():` by the desired synonyms, preserving the quotation marks. __(10)__ When investigating two specific areas,
the variables of `initialize_areas` can be rearranged. However, results can be counted manually as well. Therefore, this step is only
recommended for advanced users. An example for doing so is provided within the function.

## Part Three: Initializing names of cities, towns and villages in the Netherlands by province
The program uses twelve text files to determine which of the twelve provinces a location belongs to.
These are included in the repository. When using these, __download these and go to step 16__. By replicating the results, __keep reading__.  

__(11)__ [For each province on the Wikipedia overview page](https://en.wikipedia.org/wiki/List_of_cities,_towns_and_villages_in_the_Netherlands_by_province), [convert](https://wikitable2csv.ggor.de) the table to a CSV file.
Apply options _trim cells_ and _remove line breaks in cells_. Use `table.wikitable` as CSS selector. Download the first table. 
__(12)__ mv the downloaded files in your personal folder to folder `cities` and rename these files as follows: `drenthe.csv`, `flevoland.csv`, `friesland.csv`, `gelderland.csv`, `groningen.csv`, `limburg.csv`, `north_brabant.csv`, `north_holland.csv`, `overijssel.csv`, `south_holland.csv`, `utrecht.csv` and `zeeland.csv`. 
__(13)__ Download `cities.sh` to folder `cities`. Give execute permissions to the downloaded script using `$ chmod +x cities.sh` 
__(14)__ Run `cities.sh` using `$ ./cities.sh` in order to generate files containing names of cities, towns and villages only.
__Modifications to these files are also made by this script.__

## Part Four: Generating results

__(15)__ Cd to the parent directory containing `2018_01-06.sh` and `synonym_mapping.py`.  
__(16)__ Run the script and program using command `$ ./2018_01-06.sh | python3 synonym_mapping.py`.