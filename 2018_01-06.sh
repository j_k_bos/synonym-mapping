#!/bin/bash

## Please read the Readme file before running this script ##

# Initialize synonyms
synonym1=$"limonade"
synonym2=$"ranja"

# initialize temporary files
f201801=$(mktemp)
f201802=$(mktemp)
f201803=$(mktemp)
f201804=$(mktemp)
f201805=$(mktemp)
f201806=$(mktemp)

# append PATH so that the script can use tweet2tab
PATH=$PATH:/net/aps/haytabo/bin

# January
# get contents of compressed twitter data
zcat /net/corpora/twitter2/Tweets/2018/01/* | \
# get user-defined location and text of tweet
tweet2tab user.location text | \
# get lines with pattern synonym1 or synonym2 and 
# save these lines to the correspondingtemporary file
grep -wie $synonym1 -e $synonym2 > $f201801

# February
zcat /net/corpora/twitter2/Tweets/2018/02/* | \
tweet2tab user.location text | \
grep -wie $synonym1 -e $synonym2 > $f201802

# March
zcat /net/corpora/twitter2/Tweets/2018/03/* | \
tweet2tab user.location text | \
grep -wie $synonym1 -e $synonym2 > $f201803

# April
zcat /net/corpora/twitter2/Tweets/2018/04/* | \
tweet2tab user.location text | \
grep -wie $synonym1 -e $synonym2 > $f201804

# May
zcat /net/corpora/twitter2/Tweets/2018/05/* | \
tweet2tab user.location text | \
grep -wie $synonym1 -e $synonym2 > $f201805

# June
zcat /net/corpora/twitter2/Tweets/2018/06/* | \
tweet2tab user.location text | \
grep -wie $synonym1 -e $synonym2 > $f201806

# paste temporary files vertically to feed synonym_mapping.py
cat $f201801 $f201802 $f201803 $f201804 $f201805 $f201806

# remove temporary files
rm "$f201801" 
rm "$f201802" 
rm "$f201803" 
rm "$f201804" 
rm "$f201805" 
rm "$f201806"
